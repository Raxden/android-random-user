package com.core.features.home.usecase

import com.core.common.android.Pager
import com.core.domain.User
import com.core.domain.repository.UserRepository
import io.reactivex.Single
import javax.inject.Inject

class GetUsersPagedUseCase @Inject constructor(
    private val userRepository: UserRepository
) {

    private val ids: MutableSet<String> = mutableSetOf()

    fun execute(pager: Pager<User>): Single<List<User>> {
        return userRepository.list(pager.currentPage(), pager.pageSize())
            .map { list ->
                val mutableList = list.toMutableList()
                mutableList.removeAll { user -> ids.contains(user.id) }
                mutableList.toList()
            }
            .map { list ->
                ids.addAll(list.map { it.id })
                list
            }
            .map {
                pager.addPageData(data = it)
                pager.getAllData()
            }
            .doOnSubscribe {
                pager.setRequestPage(true)
            }
            .doOnSuccess {
                pager.setRequestPage(false)
                pager.setMoreResults(it.isNotEmpty())
            }
            .doOnError {
                pager.setRequestPage(false)
                pager.setMoreResults(false)
            }
    }
}