package com.core.data.remote.entity

import com.google.gson.annotations.SerializedName

data class ResultsEntity<T : Any>(
    @SerializedName("results")
    val results: List<T>,
    @SerializedName("info")
    val info: InfoEntity
)

data class InfoEntity(
    @SerializedName("seed")
    val seed: String,
    @SerializedName("results")
    val results: Int,
    @SerializedName("page")
    val page: Int,
    @SerializedName("version")
    val version: String
)
