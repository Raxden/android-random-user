package com.core.data.remote.entity

import com.google.gson.annotations.SerializedName

data class ErrorEntity(
    @SerializedName("error")
    val error: String? = null
)