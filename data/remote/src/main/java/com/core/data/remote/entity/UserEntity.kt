package com.core.data.remote.entity

import com.google.gson.annotations.SerializedName

data class UserEntity(
    @SerializedName("gender")
    var gender: String? = null,
    @SerializedName("name")
    var name: NameEntity? = null,
    @SerializedName("location")
    var location: LocationEntity? = null,
    @SerializedName("email")
    var email: String? = null,
    @SerializedName("login")
    var login: LoginEntity? = null,
    @SerializedName("registered")
    var registered: RegisteredEntity? = null,
    @SerializedName("phone")
    var phone: String? = null,
    @SerializedName("picture")
    var picture: PictureEntity? = null
)

data class NameEntity(
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("first")
    var first: String? = null,
    @SerializedName("last")
    var last: String? = null
)

data class LocationEntity(
    @SerializedName("street")
    var street: StreetEntity? = null,
    @SerializedName("city")
    var city: String? = null,
    @SerializedName("state")
    var state: String? = null,
    @SerializedName("country")
    var country: String? = null,
    @SerializedName("postcode")
    var postcode: String? = null,
    @SerializedName("coordinates")
    var coordinates: CoordinatesEntity? = null,
    @SerializedName("timezone")
    var timezone: TimezoneEntity? = null
)

data class StreetEntity(
    @SerializedName("number")
    var number: Int? = null,
    @SerializedName("name")
    var name: String? = null
)

data class CoordinatesEntity(
    @SerializedName("latitude")
    var latitude: String? = null,
    @SerializedName("longitude")
    var longitude: String? = null
)

data class TimezoneEntity(
    @SerializedName("offset")
    var offset: String? = null,
    @SerializedName("description")
    var description: String? = null
)

data class LoginEntity(
    @SerializedName("uuid")
    var uuid: String? = null,
    @SerializedName("username")
    var username: String? = null,
    @SerializedName("password")
    var password: String? = null,
    @SerializedName("salt")
    var salt: String? = null,
    @SerializedName("md5")
    var md5: String? = null,
    @SerializedName("sha1")
    var sha1: String? = null,
    @SerializedName("sha256")
    var sha256: String? = null
)

data class RegisteredEntity(
    @SerializedName("date")
    var date: String? = null,
    @SerializedName("age")
    var age: Int? = null
)

data class PictureEntity(
    @SerializedName("large")
    var large: String? = null,
    @SerializedName("medium")
    var medium: String? = null,
    @SerializedName("thumbnail")
    var thumbnail: String? = null
)
